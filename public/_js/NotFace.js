$(document).ready( function(){
    var root;
    var scripts = document.getElementsByTagName("script");
    var i = scripts.length;
    while (i--) {
        var match = scripts[i].src.match(/(^|.*\/)NotFace\.js$/);
        if (match) {
            root = match[1];
            break;
        }
    }

    document.getElementById("contributor_NotFace").innerHTML = `
    <div class="contributor">
        <a href="${root.replace("_js/","_contributor/NotFace.html")}"><h2 class="contributor_name">NotFace</h2></a>
            <div class="contributor_contents">
                <div class="contributor1">
                    <img class="contributor_img" src="${root.replace("_js/","_img/NotFace.png")}">
                </div>
                <div class="contributor2">
                    <p>プログラミングしている人のNotFaceです。</p>
                    <p>C#、Swift、Python、JSあたりはできます。</p>
                    <p>C++、JAVAは苦手です。</p>
                    <p>本名探したい人は勝手に探してください。</p>
                    <p>この名前は最近使い始めたものなので、本名のほうが慣れている人もいるかも？</p>
                    <p>点体望遠鏡を作っているので、使ってみてください(いまは大改造中なのでダウンロードしないほうが良いかも...)</p>
                    <a href="https://kaonasi_biwa.gitlab.io/komono/">点体望遠鏡のサイト</a><br><br>
                    <a href="${root.replace("_js/","contents.html?contributor=NotFace")}" class="blog_contributor_url">この投稿者の記事を見る</a>
                </div>
            </div>
        </div>
    `;
    });