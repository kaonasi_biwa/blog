
function serch({ contributor = "", start = 1, count = 3,tag = "" }) {

    var root;
    var scripts = document.getElementsByTagName("script");
    var i = scripts.length;
    while (i--) {
        var match = scripts[i].src.match(/(^|.*\/)contents\.js$/);
        if (match) {
            root = match[1];
            break;
        }
    }




    var conditions = true
    var html_contents = ""
    var html_count = 0
    var html_count2 = 0
    console.log(start)
    for (const json_item of contents) {
        conditions = true
        if (!(contributor == "" || contributor == json_item.contributor_id)) {
            conditions = false
        }else if(!(tag == "" || tag == null || json_item.tag.includes(tag))) {
            conditions = false
        }
         else {
            if (start - 1 > html_count) {
                conditions = false
                html_count += 1
            }
            if (count <= html_count2) {
                conditions = false
                break
            }
        }



        if (conditions) {
            html_contents += `<div class="contents">
<a href="${root.replace("_js/", json_item.url)}">
    <h2 class="contents_title">${json_item.title}</h2>

    <div class="contributor_contents">
        <div class="contributor1">
        <img class="contents_img" src="${root.replace("_js/", json_item.image)}">
        </a>
        </div>
        <div class="contributor2">
        <a>投稿者:</a><a href="${root.replace("_js/", "_contributor/" + json_item.contributor_id)}.html">${json_item.contributor}</a>
        <p>投稿日時:${json_item.post.year}/${json_item.post.month}/${json_item.post.day}</p>
        <p>更新日時:${json_item.last_update.year}/${json_item.last_update.month}/${json_item.last_update.day}</p>
        ${json_item.overview}
            <a href="${root.replace("_js/", json_item.url)}">...続きを読む</a>
        </div>
    </div>

</div>`
            html_count2 += 1
        }
    }







    return html_contents
}


function contents_count({ contributor = "",tag = "" }) {
    var _conditions = true
    var _contributor = contributor
    var _tag = tag
    var _html_count2 = 0
    for (const _json_item of contents) {
        _conditions = true
        if (!(_contributor == "" || _contributor == null || _contributor == _json_item.contributor_id)) {
            _conditions = false
        }
        if (!(_tag == "" || _tag == null || _json_item.tag.includes(_tag))) {
            _conditions = false
        }

        if (_conditions) {

            _html_count2 += 1
        }
    }







    return _html_count2
}

