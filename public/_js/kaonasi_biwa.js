$(document).ready( function(){
    var root;
    var scripts = document.getElementsByTagName("script");
    var i = scripts.length;
    while (i--) {
        var match = scripts[i].src.match(/(^|.*\/)kaonasi_biwa\.js$/);
        if (match) {
            root = match[1];
            break;
        }
    }
    document.getElementById("contributor_kaonasi_biwa").innerHTML = `
    <div class="contributor">
        <a href="${root.replace("_js/","_contributor/kaonasi_biwa.html")}"><h2 class="contributor_name">kaoansi_biwa(加尾梨 毘和)</h2></a>
        <div class="contributor_contents">
            <div class="contributor1">
                <img class="contributor_img" src="${root.replace("_js/","_img/kaonasi_biwa.png")}"">
            </div>
            <div class="contributor2">
                <p>どうも。Twitterで見かけた人もいると思いますが、kaonasi_biwaです。</p>
                <p>Minecraftやら東方やら色々好きなのでそのあたりを書いていこうかな～と思います。</p>
                <p>ということで、よろしくね～</p>
                <br>
                <p>あと、komonoDatapack配布してるので、よかったら遊んでみてください！</p>
                <a href="https://kaonasi_biwa.gitlab.io/komono/">komonoDatapackのホームページ</a><br><br>
                <a href="${root.replace("_js/","contents.html?contributor=kaonasi_biwa")}" class="blog_contributor_url">この投稿者の記事を見る</a>
            </div>
        </div>
    </div>
    `;
    });